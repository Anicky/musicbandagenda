<?php
$page_title = "Mot de passe oublié";
include_once("header.php");
?>

<section>
    <h1><?php echo $page_title; ?></h1>

    <form id="form_passwordLost">
        <table>
            <tr>
                <td class="label">
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" size="40" maxlength="100" class="validate[required]" />
                </td>
        </table>
        <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>" />
        <input class="button" type="button" onclick="goTo('')" value="Annuler" id="back" />
        <input class="button" type="submit" value="Valider" id="submit" />
    </form>
</section>

<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>

<script>
    $(function() {
        $("#form_passwordLost").validationEngine({
            scroll: false,
            onValidationComplete: function(form, status){
                if (status === true) {
                    $.ajax({
                        type : "post",
                        data : $("#form_passwordLost").serialize(),
                        url : "password_lost_action.html",
                        success : function(data){
                            $("#dialogbox").html(data);
                            $("#dialogbox").dialog("open");
                        },
                        error : function(){
                            $("#dialogbox").html('Une erreur est survenue.');
                            $("#dialogbox").dialog("open");
                        }
                    });
                }
            } 
        });
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Connexion",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>

<?php include_once("footer.php"); ?>