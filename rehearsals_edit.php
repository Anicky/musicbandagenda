<?php
require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {

        $request_rehearsals = "SELECT place, cost FROM rehearsals WHERE id = ?";
        $response_rehearsals = $db->prepare($request_rehearsals);
        $response_rehearsals->bindValue(1, $id, PDO::PARAM_INT);
        $response_rehearsals->execute();
        $data_rehearsals = $response_rehearsals->fetch();
        if ($data_rehearsals != null) {
            $place = securite_sortie($data_rehearsals['place']);
            $cost = securite_sortie($data_rehearsals['cost']);
            ?>
            <form id="form_rehearsal">
                <label for="place">Lieu</label><br />
                <input type="text" name="place" id="place" size="40" maxlength="100" value="<?php echo $place; ?>" /><br /><br />
                <label for="cost">Coût</label><br />
                <input type="number" name="cost" id="cost" size="10" maxlength="10" value="<?php echo $cost; ?>" /> €
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>" />
            </form>
            <script>
                $("#dialogbox").dialog('option', 'buttons', { 
                    "Annuler" : function() {
                        $(this).dialog("close");
                    },
                    "Valider" : function() {
                        $("#form_rehearsal").submit();
                    }
                });
                $(function() {
                    $("#form_rehearsal").validationEngine({
                        scroll: false,
                        onValidationComplete: function(form, status){
                            if (status === true) {
                                $.ajax({
                                    type : "post",
                                    data : $("#form_rehearsal").serialize(),
                                    url : "rehearsals_edit_action.html",
                                    success : function(data){
                                        $("#dialogbox").dialog('option', 'buttons', { 
                                            "Fermer" : function() {
                                                $(this).dialog("close");
                                            }
                                        });
                                        $("#dialogbox").html(data);
                                    },
                                    error : function(){
                                        $("#dialogbox").html('Une erreur est survenue.');
                                    }
                                });
                            }
                        } 
                    });
                });
            </script>
            <?php
        } else {
            ?>
            <p>La répétition recherchée est introuvable.</p>
            <?php
        }
        $response_rehearsals->closeCursor();
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php
    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
}
?>