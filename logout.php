<?php

require_once("config.php");
if (isset($_SESSION['user'])) {
    $request_user = "UPDATE members SET token = NULL WHERE id = ?";
    $response_user = $db->prepare($request_user);
    $response_user->bindValue(1, $session_id, PDO::PARAM_INT);
    $response_user->execute();
    $response_user->closeCursor();
    session_destroy();
    header('Location: ./');
    exit;
} else {
    header('Location: ./');
    exit;
}
?>