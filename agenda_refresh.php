<?php

require_once("config.php");
require_once("access_admin.php");

if (isset($_POST['token_validation'])) {
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        $request_refresh = "DELETE FROM availabilities WHERE date_end < '" . date("Y-m-d H:i:s") . "'";
        $response_refresh = $db->prepare($request_refresh);
        $response_refresh->execute();
        $response_refresh->closeCursor();
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>