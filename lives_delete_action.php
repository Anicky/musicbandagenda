<?php

require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    ?>
    <script>
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
    </script>
    <?php

    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {
        $request_lives = "DELETE FROM lives WHERE id = ?";
        $response_lives = $db->prepare($request_lives);
        $response_lives->bindValue(1, $id, PDO::PARAM_INT);
        $response_lives->execute();
        $response_lives->closeCursor();
        ?>
        <script>
            $.get("lives_list.html", {}, function(data) {
                $("#lives").html(data);
            });
            calendar.fullCalendar('refetchEvents');
        </script>
        <p>Le concert a bien été supprimé.</p>
        <?php

    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>