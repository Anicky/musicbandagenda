<?php

require_once("config.php");

if (isset($_POST['email'])) {
    // Variables
    $email = $_POST['email'];
    $password = $_POST['password'];
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if (($email != "") && ($password != "")) {
            $request_login = "SELECT members.id, password, roles.label AS nomRole, active
                FROM members, roles
                WHERE members.role_fk = roles.id
                AND members.email = ?";
            $response_login = $db->prepare($request_login);
            $response_login->bindValue(1, $email, PDO::PARAM_STR);
            $response_login->execute();
            $data_login = $response_login->fetch();
            $ok = false;
            $token = "";
            if ($data_login != null) {
                if (testerMotDePasse($password, $data_login['password']) == $data_login['password'] && ($data_login['active'])) {
                    $ok = true;
                    $token = create_token();
                    $request_token = "UPDATE members SET token = ? WHERE id = ?";
                    $response_token = $db->prepare($request_token);
                    $response_token->bindValue(1, $token, PDO::PARAM_STR);
                    $response_token->bindValue(2, $data_login['id'], PDO::PARAM_INT);
                    $response_token->execute();
                    $response_token->closeCursor();
                }
            }
            $response_login->closeCursor();
            if ($ok) {
                $_SESSION['user'] = $token;
                ?>
                <p>Vous êtes maintenant connecté !</p>
                <script>
                    window.location.href = "./";
                </script>
                <?php

            } else {
                ?>
                <p>
                    - Votre login et/ou votre mot de passe sont peut-être incorrects.<br />
                    - Votre compte est peut-être inactif.
                </p>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>