<?php
if (!isset($db)) {
    require_once("config.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <base href="<?php echo URL; ?>" />
        <meta name="description" content="<?php echo get_parameter($db, "meta_description"); ?>" />
        <meta name="keywords" content="<?php echo get_parameter($db, "meta_keywords"); ?>" />
        <link rel="stylesheet" href="css/default.css" />
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <link rel="stylesheet" href="css/jquery-fullcalendar.css" />
        <link rel="stylesheet" href="css/jquery-fullcalendar-print.css" media="print" />
        <link rel="stylesheet" href="css/jquery-fullcalendar-colors.css" />
        <link rel="stylesheet" href="css/jquery-validationEngine.css" />
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/jquery-ui.js" charset="utf-8"></script>
        <script src="js/jquery-validationEngineFR.js" charset="utf-8"></script>
        <script src="js/jquery-validationEngine.js" charset="utf-8"></script>
        <script src="js/jquery-fullcalendar.js" charset="utf-8"></script>
        <!--[if lt IE 9]><script src="js/html5.js" charset="utf-8"></script><![endif]-->
        <script>
            $(function() {
                $.ajaxSetup ({
                    cache: false
                });
            });
            function goTo(url) {
                window.location.href = "<?php echo URL; ?>" + url;
            }
        </script>
        <title><?php echo get_parameter($db, "general_title"); ?><?php
if (isset($page_title)) {
    echo " - " . $page_title;
}
?></title>
    </head>
    <body>
        <header>
            <div class="header_wrap">
                <a href="" title="Revenir à l'accueil du site" class="button left">
                    <?php echo get_parameter($db, "general_title"); ?>
                </a>
                <?php
                if (isset($_SESSION['user'])) {
                    ?>
                    <div id="username">
                        <a href="" title="Revenir à l'accueil du site"><?php echo $session_firstname . " " . strtoupper($session_lastname) ;?></a>
                    </div>
                    <?php
                }
                ?>
                <nav>
                    <?php include_once("menu.php"); ?> 
                </nav>
                <div class="clear"></div>
            </div>
        </header>