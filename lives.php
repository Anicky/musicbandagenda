<?php
$page_title = "Concerts";
require_once("access.php");
include_once("header.php");
?>

<section>
    <h1><?php echo $page_title; ?></h1>
    <div id="lives">
        <?php
        require_once("lives_list.php");
        ?>
    </div>
</section>

<?php
if ($session_role == "admin") {
    ?>
    <section>
        <div id="calendrier"></div>
    </section>
    <div id="dialogbox"></div>
    <script>
        var token = '<?php echo $_SESSION['token_validation']; ?>';
        var calendar = null;
        $(function() {
            $("#dialogbox").dialog({
                autoOpen : false,
                modal : true,
                width : 700,
                resizable : false,
                draggable : false,
                show : "fade",
                hide : "fade",
                title : "Concert",
                buttons : {
                    "Fermer" : function() {
                        $(this).dialog("close");
                    }
                }
            });
            calendar = $('#calendrier').fullCalendar({
                buttonText: {
                    today: "Aujourd'hui",
                    month: "Mois",
                    week: "Semaine",
                    day: "Jour"},
                defaultView: 'agendaWeek',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                selectable: true,
                theme : true,
                monthNames:
                    ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
                monthNamesShort:
                    ['janv.','févr.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'],
                dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
                dayNamesShort: ['Dim','Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                titleFormat: {
                    month: 'MMMM yyyy', // ex : Janvier 2010
                    week: "d[ MMMM][ yyyy]{ - d MMMM yyyy}", // ex : 10 — 16 Janvier 2010, semaine à cheval : 28 Décembre 2009 - 3 Janvier 2010
                    // todo : ajouter le numéro de la semaine
                    day: 'dddd d MMMM yyyy' // ex : Jeudi 14 Janvier 2010
                },
                columnFormat: {
                    month: 'ddd', // Ven.
                    week: 'ddd d', // Ven. 15
                    day: '' // affichage déja complet au niveau du 'titleFormat'
                },
                axisFormat: 'H:mm', // la demande de ferdinand.amoi : 15:00 (pour 15, simplement supprimer le ':mm'
                timeFormat: {
                    '': 'H:mm', // événements vue mensuelle.
                    agenda: 'H:mm{ - H:mm}' // événements vue agenda
                },
                firstDay:1, // Lundi premier jour de la semaine 
                selectHelper: true,
                select : function(start, end, allDay) {
                    calendar.fullCalendar('unselect');
                    $.ajax({
                        type: "post",
                        url: "lives_set.html",
                        data: 'start=' + $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss") +
                            '&end=' + $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss") +
                            '&token_validation=' + token,
                        success : function() {
                            calendar.fullCalendar('refetchEvents');
                            $.get("lives_list.html", {}, function(data) {
                                $("#lives").html(data);
                            });
                        }
                    });
                },
                eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
                    $.ajax({
                        type: "post",
                        url: "lives_set.html",
                        data: 'id=' +event.id +
                            '&start=' + $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss") +
                            '&end=' + $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss") +
                            '&token_validation=' + token,
                        success : function() {
                            calendar.fullCalendar('refetchEvents');
                            $.get("lives_list.html", {}, function(data) {
                                $("#lives").html(data);
                            });
                        }
                    });
                },
                eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                    $.ajax({
                        type: "post",
                        url: "lives_set.html",
                        data: 'id=' +event.id +
                            '&start=' + $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss") +
                            '&end=' + $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss") +
                            '&token_validation=' + token,
                        success : function() {
                            calendar.fullCalendar('refetchEvents');
                            $.get("lives_list.html", {}, function(data) {
                                $("#lives").html(data);
                            });
                        }
                    });
                },
                editable: true,
                allDayDefault : false,
                events: "lives_get.html"
            });
        });
        function live_edit(id) {
            $("#dialogbox").dialog('option', 'buttons', {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            });
            $.post("lives_edit.html", {id : id, token_validation : token}, function(data) {
                $("#dialogbox").html(data);
                $("#dialogbox").dialog("open");
            });
        }
        function live_delete(id) {
            $("#dialogbox").dialog('option', 'buttons', {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            });
            $.post("lives_delete.html", {id : id, token_validation : token}, function(data) {
                $("#dialogbox").html(data);
                $("#dialogbox").dialog("open");
            });
        }
    </script>
    <?php
}
?>

<?php include_once("footer.php"); ?>