<?php

require_once("config.php");

if (isset($_POST['email'])) {
    // Variables
    $email = $_POST['email'];
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if ($email != "") {

            $request_password = "SELECT * FROM members WHERE email = ?";
            $response_password = $db->prepare($request_password);
            $response_password->bindValue(1, $email, PDO::PARAM_STR);
            $response_password->execute();
            $data_password = $response_password->fetch();
            if ($data_password != null) {
                $password = $data_password['password'];
                $firstname = $data_password['firstname'];
                $lastname = $data_password['lastname'];
                ?>
                <script>
                    $("#dialogbox").bind('dialogclose', function() {
                        window.location.href = "./";
                    });
                </script>
                <?php

                if (mail_password_lost($email, $firstname, $lastname, $password, get_parameter($db, "general_title"))) {
                    ?>
                    <p>Un mail pour réinitialiser votre mot de passe vient de vous être envoyé.</p>
                    <?php

                } else {
                    ?>
                    <p>Le mail de réinitialisation de mot de passe n'a pas pu être envoyé.</p>
                    <?php

                }
            } else {
                ?>
                <p>Impossible de trouver votre email.</p>
                <?php

            }
            $response_password->closeCursor();
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>