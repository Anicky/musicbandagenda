<?php
require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {
        ?>
        <script>
            $("#dialogbox").dialog('option', 'buttons', { 
                "Non" : function() {
                    $(this).dialog("close");
                },
                "Oui" : function() {
                    $.post("rehearsals_delete_action.html", {id : '<?php echo $id; ?>', token_validation : '<?php echo $_POST['token_validation']; ?>'}, function(html) {
                        $("#dialogbox").dialog('option', 'buttons', {
                            "Fermer" : function() {
                                $(this).dialog("close");
                            }
                        });
                        $("#dialogbox").html(html);
                    });
                }
            });
        </script>
        <p>Etes-vous sûr de vouloir supprimer cette répétition ?</p>
        <?php
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php
    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
}
?>