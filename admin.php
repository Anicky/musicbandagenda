<?php
$page_title = "Administration";
include_once("header.php");
require_once("access_admin.php");
?>

<section>
    <h1><?php echo $page_title; ?></h1>

    <form id="form_admin">
        <table>
            <tr>
                <td class="label">
                    <label for="general_title">Titre du site</label>
                </td>
                <td>
                    <input type="text" name="general_title" id="general_title" size="70" maxlength="100" class="validate[required]" value="<?php echo securite_sortie(get_parameter($db, "general_title")); ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="content_footer">Pied de page</label>
                </td>
                <td>
                    <textarea name="content_footer" id="content_footer" class="validate[required]" cols="50" rows="5"><?php echo securite_sortie(get_parameter($db, "content_footer")); ?></textarea>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="meta_description">Méta-informations : description du site</label>
                </td>
                <td>
                    <textarea name="meta_description" id="meta_description" class="validate[required]" cols="50" rows="5"><?php echo securite_sortie(get_parameter($db, "meta_description")); ?></textarea>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="meta_keywords">Méta-informations : mots-clés du site</label>
                </td>
                <td>
                    <textarea name="meta_keywords" id="meta_keywords" class="validate[required]" cols="50" rows="5"><?php echo securite_sortie(get_parameter($db, "meta_keywords")); ?></textarea>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="css_calendar_colors">CSS : couleurs du calendrier</label>
                </td>
                <td>
                    <textarea name="css_calendar_colors" id="css_calendar_colors" class="validate[required]" cols="50" rows="22"><?php
$fichier = fopen("css/jquery-fullcalendar-colors.css", "r");
while ($ligne = fgets($fichier, 1024)) {
    echo $ligne;
}
fclose($fichier);
?></textarea>
                </td>
            </tr>
        </table>
        <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>" />
        <input class="button" type="button" onclick="goTo('')" value="Annuler" id="back" />
        <input class="button" type="submit" value="Valider" id="submit" />
    </form>
</section>

<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>

<script>
    $(function() {
        $("#form_admin").validationEngine({
            scroll: false,
            onValidationComplete: function(form, status){
                if (status === true) {
                    $.ajax({
                        type : "post",
                        data : $("#form_admin").serialize(),
                        url : "admin_action.html",
                        success : function(data){
                            $("#dialogbox").html(data);
                            $("#dialogbox").dialog("open");
                        },
                        error : function(){
                            $("#dialogbox").html('Une erreur est survenue.');
                            $("#dialogbox").dialog("open");
                        }
                    });
                }
            } 
        });
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Administration",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>

<?php include_once("footer.php"); ?>