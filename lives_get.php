<?php

require_once("config.php");
require_once("access_admin.php");

$request_agenda = "SELECT id, date_start, date_end, place, gain FROM lives ORDER BY date_start DESC";
$response_agenda = $db->prepare($request_agenda);
$response_agenda->execute();
$lives = array();
$i = 0;
while ($data_agenda = $response_agenda->fetch()) {
    $lives[$i]['id'] = $data_agenda['id'];
    $lives[$i]['title'] = "Concert\n" . $data_agenda['place'] . "\n" . $data_agenda['gain'] . " €";
    $lives[$i]['start'] = $data_agenda['date_start'];
    $lives[$i]['end'] = $data_agenda['date_end'];
    $lives[$i]['className'] = "live";
    $lives[$i]['editable'] = true;
    $i++;
}
$response_agenda->closeCursor();

echo json_encode($lives);
?>
