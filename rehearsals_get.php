<?php

require_once("config.php");
require_once("access_admin.php");

$request_agenda = "SELECT id, date_start, date_end, place, cost FROM rehearsals ORDER BY date_start DESC";
$response_agenda = $db->prepare($request_agenda);
$response_agenda->execute();
$rehearsals = array();
$i = 0;
while ($data_agenda = $response_agenda->fetch()) {
    $rehearsals[$i]['id'] = $data_agenda['id'];
    $rehearsals[$i]['title'] = "Répétition\n" . $data_agenda['place'] . "\n" . $data_agenda['cost'] . " €";
    $rehearsals[$i]['start'] = $data_agenda['date_start'];
    $rehearsals[$i]['end'] = $data_agenda['date_end'];
    $rehearsals[$i]['className'] = "rehearsal";
    $rehearsals[$i]['editable'] = true;
    $i++;
}
$response_agenda->closeCursor();

echo json_encode($rehearsals);
?>
