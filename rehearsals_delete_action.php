<?php

require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    ?>
    <script>
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
    </script>
    <?php

    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {
        $request_rehearsals = "DELETE FROM rehearsals WHERE id = ?";
        $response_rehearsals = $db->prepare($request_rehearsals);
        $response_rehearsals->bindValue(1, $id, PDO::PARAM_INT);
        $response_rehearsals->execute();
        $response_rehearsals->closeCursor();
        ?>
        <script>
            $.get("rehearsals_list.html", {}, function(data) {
                $("#rehearsals").html(data);
            });
            calendar.fullCalendar('refetchEvents');
        </script>
        <p>La répétition a bien été supprimée.</p>
        <?php

    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>