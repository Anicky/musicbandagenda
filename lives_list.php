<?php
require_once("access.php");
if (!isset($db)) {
    require_once("config.php");
}

$members_count = 0;
$request_members = "SELECT COUNT(*) AS members_count FROM members";
$response_members = $db->query($request_members);
$data_members = $response_members->fetch();
if ($data_members != null) {
    $members_count = $data_members['members_count'];
}
$response_members->closeCursor();
?>

<h2>Concerts prévus</h2>

<?php
$request_lives = "SELECT id,
        DATE_FORMAT(date_start, '%d/%m/%Y') AS date_live,
        DATE_FORMAT(date_start, '%Hh%i') AS hour_start,
        DATE_FORMAT(date_end, '%Hh%i') AS hour_end,
        gain,
        place,
        TIME_FORMAT(TIMEDIFF(date_end, date_start), '%Hh%i') AS length
        FROM lives WHERE date_start >= '" . date("Y-m-d H:i:s") . "' ORDER BY date_start ASC";
$response_lives = $db->query($request_lives);
$lives_count = $response_lives->rowCount();
if ($lives_count > 0) {
    ?>
    <table class="cells">
        <tr>
            <th>Date</th>
            <th>Heure de début</th>
            <th>Heure de fin</th>
            <th id="place">Lieu</th>
            <th>Durée</th>
            <th>Gain total</th>
            <th>Gain par personne</th>
            <?php
            if ($session_role == "admin") {
                ?>
                <th></th>
                <th></th>
                <?php
            }
            ?>
        </tr>
        <?php
        while ($data_lives = $response_lives->fetch()) {
            ?>
            <tr>
                <td>
                    <?php echo $data_lives['date_live']; ?>
                </td>
                <td>
                    <?php echo $data_lives['hour_start']; ?>
                </td>
                <td>
                    <?php echo $data_lives['hour_end']; ?>
                </td>
                <td>
                    <?php echo $data_lives['place']; ?>
                </td>
                <td>
                    <?php echo $data_lives['length']; ?>
                </td>
                <td>
                    <?php echo str_replace('.', ',', $data_lives['gain']); ?> €
                </td>
                <td>
                    <?php echo str_replace('.', ',', ($data_lives['gain'] / $members_count)); ?> €
                </td>
                <?php
                if ($session_role == "admin") {
                    ?>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Modifier la répétition" onclick="live_edit('<?php echo $data_lives['id']; ?>')">
                            <img src="img/icon_edit.png" alt="" />
                        </a>
                    </td>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Supprimer la répétition" onclick="live_delete('<?php echo $data_lives['id']; ?>')">
                            <img src="img/icon_delete.png" alt="" />
                        </a>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        $response_lives->closeCursor();
        ?>
    </table>
    <?php
} else {
    ?>
    <p>Pas de concerts prévus pour le moment.</p>
    <?php
}
?>

<h2>Concerts passés</h2>

<?php
$request_lives = "SELECT id,
        DATE_FORMAT(date_start, '%d/%m/%Y') AS date_live,
        DATE_FORMAT(date_start, '%Hh%i') AS hour_start,
        DATE_FORMAT(date_end, '%Hh%i') AS hour_end,
        gain,
        place,
        TIME_FORMAT(TIMEDIFF(date_end, date_start), '%Hh%i') AS length
        FROM lives WHERE date_end < '" . date("Y-m-d H:i:s") . "' ORDER BY date_end DESC";
$response_lives = $db->query($request_lives);
$lives_count = $response_lives->rowCount();
if ($lives_count > 0) {
    ?>
    <table class="cells">
        <tr>
            <th>Date</th>
            <th>Heure de début</th>
            <th>Heure de fin</th>
            <th id="place">Lieu</th>
            <th>Durée</th>
            <th>Gain total</th>
            <th>Gain par personne</th>
            <?php
            if ($session_role == "admin") {
                ?>
                <th></th>
                <th></th>
                <?php
            }
            ?>
        </tr>
        <?php
        while ($data_lives = $response_lives->fetch()) {
            ?>
            <tr>
                <td>
                    <?php echo $data_lives['date_live']; ?>
                </td>
                <td>
                    <?php echo $data_lives['hour_start']; ?>
                </td>
                <td>
                    <?php echo $data_lives['hour_end']; ?>
                </td>
                <td>
                    <?php echo $data_lives['place']; ?>
                </td>
                <td>
                    <?php echo $data_lives['length']; ?>
                </td>
                <td>
                    <?php echo str_replace('.', ',', $data_lives['gain']); ?> €
                </td>
                <td>
                    <?php echo str_replace('.', ',', ($data_lives['gain'] / $members_count)); ?> €
                </td>
                <?php
                if ($session_role == "admin") {
                    ?>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Modifier la répétition" onclick="live_edit('<?php echo $data_lives['id']; ?>')">
                            <img src="img/icon_edit.png" alt="" />
                        </a>
                    </td>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Supprimer la répétition" onclick="live_delete('<?php echo $data_lives['id']; ?>')">
                            <img src="img/icon_delete.png" alt="" />
                        </a>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        $response_lives->closeCursor();
        ?>
    </table>
    <?php
} else {
    ?>
    <p>Pas de concerts passés.</p>
    <?php
}
?>
<h2>Gain des concerts</h2>
<?php
$lives_gain_total = 0;
$lives_gain_unit = 0;
$request_lives = "SELECT
        SUM(gain) AS gain_total
        FROM lives
        WHERE date_end < '" . date("Y-m-d H:i:s") . "'";
$response_lives = $db->query($request_lives);
$data_lives = $response_lives->fetch();
if ($data_lives != null) {
    $lives_gain_total = $data_lives['gain_total'];
    if ($lives_gain_total == "") {
        $lives_gain_total = 0;
    }
    $lives_gain_unit = $lives_gain_total / $members_count;
}
$response_lives->closeCursor();
?>
<table>
    <tr>
        <td class="label">
            Gain total :
        </td>
        <td>
            <strong><?php echo str_replace('.', ',', $lives_gain_total); ?> €</strong>
        </td>
    </tr>
    <tr>
        <td class="label">
            Gain par personne :
        </td>
        <td>
            <strong><?php echo str_replace('.', ',', $lives_gain_unit); ?> €</strong>
        </td>
    </tr>
</table>