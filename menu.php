<?php
if (!isset($db)) {
    require_once("config.php");
}
if (isset($_SESSION['user'])) {
    ?>
    <ul>
        <li>
            <a href="" title="Accéder à l'agenda" class="button">
                <img src="img/icon_calendar.png" alt="" />
                Agenda
            </a>
        </li>
        <li>
            <a href="rehearsals.html" title="Voir les répétitions passées et prévues" class="button">
                <img src="img/icon_rehearsal.png" alt="" />
                Répet'
            </a>
        </li>
        <li>
            <a href="lives.html" title="Voir les concerts passés et prévus" class="button">
                <img src="img/icon_live.png" alt="" />
                Concerts
            </a>
        </li>
        <?php
        if ($session_role == "admin") {
            ?>
            <li>
                <a href="admin.html" title="Administration" class="button">
                    <img src="img/icon_admin.png" alt="" />
                    Admin
                </a>
            </li>
            <?php
        }
        ?>
        <li>
            <a href="options.html" title="Accéder aux paramètres" class="button">
                <img src="img/icon_options.png" alt="" />
                Paramètres
            </a>
        </li>
        <li>
            <a href="logout.html" title="Se déconnecter" class="button">
                <img src="img/icon_logout.png" alt="" />
                Déconnexion
            </a>
        </li>
    </ul>
    <?php
}
?>