<?php

require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    ?>
    <script>
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
    </script>
    <?php

    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {
        $request_lives = "UPDATE lives SET place = ?, gain = ? WHERE id = ?";
        $response_lives = $db->prepare($request_lives);
        $response_lives->bindValue(1, $_POST['place'], PDO::PARAM_STR);
        $response_lives->bindValue(2, $_POST['gain'], PDO::PARAM_STR);
        $response_lives->bindValue(3, $id, PDO::PARAM_INT);
        $response_lives->execute();
        $response_lives->closeCursor();
        ?>
        <script>
            $.get("lives_list.html", {}, function(data) {
                $("#lives").html(data);
            });
            calendar.fullCalendar('refetchEvents');
        </script>
        <p>Le concert a bien été modifié.</p>
        <?php

    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>