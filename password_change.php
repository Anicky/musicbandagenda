<?php
require_once("config.php");
if (!isset($_SESSION['user'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
} else {
    ?>

    <form id="form_passwordChange">
        <label for="password_old">Mot de passe actuel</label><br />
        <input type="password" name="password_old" id="password_old" size="40" maxlength="100" class="validate[required]" /><br /><br />
        <label for="password_new1">Nouveau mot de passe</label><br />
        <input type="password" name="password_new1" id="password_new1" size="40" maxlength="100" class="validate[required]" /><br /><br />
        <label for="password_new2">Confirmation du nouveau mot de passe</label><br />
        <input type="password" name="password_new2" id="password_new2" size="40" maxlength="100" class="validate[required]" />
        <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>" />
    </form>
    <script>
        $("#dialogbox2").dialog('option', 'buttons', { 
            "Annuler" : function() {
                $(this).dialog("close");
            },
            "Valider" : function() {
                $("#form_passwordChange").submit();
            }
        });
        $(function() {
            $("#form_passwordChange").validationEngine({
                scroll: false,
                onValidationComplete: function(form, status){
                    if (status === true) {
                        $.ajax({
                            type : "post",
                            data : $("#form_passwordChange").serialize(),
                            url : "password_change_action.html",
                            success : function(data){
                                $("#dialogbox2").dialog('option', 'buttons', { 
                                    "Fermer" : function() {
                                        $(this).dialog("close");
                                    }
                                });
                                $("#dialogbox2").html(data);
                            },
                            error : function(){
                                $("#dialogbox2").html('Une erreur est survenue.');
                            }
                        });
                    }
                } 
            });
        });
    </script>
    <?php
}
?>