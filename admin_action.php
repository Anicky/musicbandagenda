<?php

require_once("config.php");
require_once("access_admin.php");

if (isset($_POST['general_title'])) {
    // Variables
    $general_title = securite_bdd($_POST['general_title']);
    $content_footer = securite_bdd($_POST['content_footer']);
    $meta_description = securite_bdd($_POST['meta_description']);
    $meta_keywords = securite_bdd($_POST['meta_keywords']);
    $css_calendar_colors = securite_bdd($_POST['css_calendar_colors']);
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if (($general_title != "") && ($content_footer != "") && ($meta_description != "") && ($meta_keywords != "") && ($css_calendar_colors != "")) {
            // CSS : couleurs du calendrier
            $fichier = fopen("css/jquery-fullcalendar-colors.css", "r+");
            ftruncate($fichier, 0);
            fwrite($fichier, $css_calendar_colors);
            fclose($fichier);
            // Titre du site
            $request_admin = "UPDATE parameters SET value = ? WHERE label = 'general_title'";
            $response_admin = $db->prepare($request_admin);
            $response_admin->bindValue(1, $general_title, PDO::PARAM_STR);
            $response_admin->execute();
            $response_admin->closeCursor();
            // Footer
            $request_admin = "UPDATE parameters SET value = ? WHERE label = 'content_footer'";
            $response_admin = $db->prepare($request_admin);
            $response_admin->bindValue(1, $content_footer, PDO::PARAM_STR);
            $response_admin->execute();
            $response_admin->closeCursor();
            // Méta-informations : description
            $request_admin = "UPDATE parameters SET value = ? WHERE label = 'meta_description'";
            $response_admin = $db->prepare($request_admin);
            $response_admin->bindValue(1, $meta_description, PDO::PARAM_STR);
            $response_admin->execute();
            $response_admin->closeCursor();
            // Méta-informations : mots-clés
            $request_admin = "UPDATE parameters SET value = ? WHERE label = 'meta_keywords'";
            $response_admin = $db->prepare($request_admin);
            $response_admin->bindValue(1, $meta_keywords, PDO::PARAM_STR);
            $response_admin->execute();
            $response_admin->closeCursor();
            ?>
            <p>Les paramètres ont bien été sauvegardés.</p>
            <script>
                $("#dialogbox").bind('dialogclose', function() {
                    window.location.href = "admin.html";
                });
            </script>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>