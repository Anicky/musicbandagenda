<?php

require_once("access.php");
if (!isset($db)) {
    require_once("config.php");
}

$request_agenda = "SELECT members.id AS id_member, availabilities.id AS id_availability, date_start, date_end, firstname, lastname
    FROM availabilities, members
    WHERE availabilities.member_fk = members.id
    ORDER BY date_start DESC";
$response_agenda = $db->prepare($request_agenda);
$response_agenda->execute();
$availabilities = array();
$i = 0;
while ($data_agenda = $response_agenda->fetch()) {
    $availabilities[$i]['member'] = $data_agenda['id_member'];
    $availabilities[$i]['id'] = $data_agenda['id_availability'];
    $availabilities[$i]['title'] = $data_agenda['firstname'];
    $availabilities[$i]['start'] = $data_agenda['date_start'];
    $availabilities[$i]['end'] = $data_agenda['date_end'];
    $availabilities[$i]['className'] = "user_" . $data_agenda['id_member'];
    if ($session_id == $data_agenda['id_member']) {
        $availabilities[$i]['editable'] = true;
    } else {
        $availabilities[$i]['editable'] = false;
    }
    $i++;
}
$response_agenda->closeCursor();

$request_agenda = "SELECT id, date_start, date_end, place, cost FROM rehearsals ORDER BY date_start DESC";
$response_agenda = $db->prepare($request_agenda);
$response_agenda->execute();
while ($data_agenda = $response_agenda->fetch()) {
    $availabilities[$i]['title'] = "Répétition\n" . $data_agenda['place'] . "\n" . $data_agenda['cost'] . " €";
    $availabilities[$i]['start'] = $data_agenda['date_start'];
    $availabilities[$i]['end'] = $data_agenda['date_end'];
    $availabilities[$i]['className'] = "rehearsal";
    $availabilities[$i]['editable'] = false;
    $i++;
}
$response_agenda->closeCursor();

$request_agenda = "SELECT id, date_start, date_end, place, gain FROM lives ORDER BY date_start DESC";
$response_agenda = $db->prepare($request_agenda);
$response_agenda->execute();
while ($data_agenda = $response_agenda->fetch()) {
    $availabilities[$i]['title'] = "Concert\n" . $data_agenda['place'] . "\n" . $data_agenda['gain'] . " €";
    $availabilities[$i]['start'] = $data_agenda['date_start'];
    $availabilities[$i]['end'] = $data_agenda['date_end'];
    $availabilities[$i]['className'] = "live";
    $availabilities[$i]['editable'] = false;
    $i++;
}
$response_agenda->closeCursor();

echo json_encode($availabilities);
?>
