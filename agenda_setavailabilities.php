<?php

require_once("access.php");
require_once("config.php");

if (isset($_POST['start'])) {
    // Variables
    $start = $_POST['start'];
    $end = $_POST['end'];
    $id = null;
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if (($start != "") && ($end != "")) {
            if ($id == null) {
                // Ajout
                $request_availability = "INSERT INTO availabilities(member_fk, date_start, date_end) VALUES (?, ?, ?)";
                $response_availability = $db->prepare($request_availability);
                $response_availability->bindValue(1, $session_id, PDO::PARAM_INT);
                $response_availability->bindValue(2, $start, PDO::PARAM_STR);
                $response_availability->bindValue(3, $end, PDO::PARAM_STR);
                $response_availability->execute();
                $response_availability->closeCursor();
            } else {
                // Modification
                $ok = false;
                $request_availability = "SELECT member_fk FROM availabilities WHERE id = ?";
                $response_availability = $db->prepare($request_availability);
                $response_availability->bindValue(1, $id, PDO::PARAM_INT);
                $response_availability->execute();
                $data_availability = $response_availability->fetch();
                if ($data_availability != null) {
                    echo $data_availability['member_fk'];
                    if ($data_availability['member_fk'] == $session_id) {
                        $ok = true;
                    }
                }
                $response_availability->closeCursor();
                if ($ok) {
                    $request_availability = "UPDATE availabilities SET date_start = ?, date_end = ? WHERE id = ?";
                    $response_availability = $db->prepare($request_availability);
                    $response_availability->bindValue(1, $start, PDO::PARAM_STR);
                    $response_availability->bindValue(2, $end, PDO::PARAM_STR);
                    $response_availability->bindValue(3, $id, PDO::PARAM_INT);
                    $response_availability->execute();
                    $response_availability->closeCursor();
                }
            }
        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>