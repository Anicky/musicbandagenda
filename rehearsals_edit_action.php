<?php

require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    ?>
    <script>
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
    </script>
    <?php

    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {
        $request_rehearsals = "UPDATE rehearsals SET place = ?, cost = ? WHERE id = ?";
        $response_rehearsals = $db->prepare($request_rehearsals);
        $response_rehearsals->bindValue(1, $_POST['place'], PDO::PARAM_STR);
        $response_rehearsals->bindValue(2, $_POST['cost'], PDO::PARAM_STR);
        $response_rehearsals->bindValue(3, $id, PDO::PARAM_INT);
        $response_rehearsals->execute();
        $response_rehearsals->closeCursor();
        ?>
        <script>
            $.get("rehearsals_list.html", {}, function(data) {
                $("#rehearsals").html(data);
            });
            calendar.fullCalendar('refetchEvents');
        </script>
        <p>La répétition a bien été modifiée.</p>
        <?php

    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>