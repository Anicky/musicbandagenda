<?php

require_once("config.php");

if (isset($_POST['password_old'])) {
    // Variables
    $password_old = $_POST['password_old'];
    $password_new1 = $_POST['password_new1'];
    $password_new2 = $_POST['password_new2'];
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if (($password_old != "") && ($password_new1 != "") && ($password_new2 != "")) {
            if ($password_new1 == $password_new2) {

                $request_password = "SELECT firstname, lastname, password, email FROM members WHERE id = ?";
                $response_password = $db->prepare($request_password);
                $response_password->bindValue(1, $session_id, PDO::PARAM_INT);
                $response_password->execute();
                $data_password = $response_password->fetch();
                if ($data_password != null) {
                    $firstname = $data_password['firstname'];
                    $lastname = $data_password['lastname'];
                    $email = $data_password['email'];

                    if (testerMotDePasse($password_old, $data_password['password']) == $data_password['password']) {
                        $password = crypter($password_new1);
                        $request_change_password = "UPDATE members SET password = ? WHERE id = ?";
                        $response_change_password = $db->prepare($request_change_password);
                        $response_change_password->bindValue(1, $password, PDO::PARAM_STR);
                        $response_change_password->bindValue(2, $session_id, PDO::PARAM_INT);
                        $response_change_password->execute();
                        $response_change_password->closeCursor();
                        ?>
                        <p>Votre mot de passe a bien été modifié.</p>
                        <?php
                        mail_password_change($email, $firstname, $lastname, $password_new1, get_parameter($db, "general_title"));
                    } else {
                        ?>
                        <p>L'ancien mot de passe que vous avez indiqué ne correspond pas à votre mot de passe actuel.</p>
                        <?php

                    }
                } else {
                    ?>
                    <p>Impossible de trouver votre compte.</p>
                    <?php

                }
                $response_password->closeCursor();
            } else {
                ?>
                <p>Le mot de passe de confirmation que vous avez entré ne correspond pas au nouveau mot de passe demandé.</p>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>