<?php
$page_title = "Page introuvable";
include_once("header.php");
?>

<section>
    <h1><?php echo $page_title; ?></h1>

    <p>
        Erreur : la page que vous recherchez est introuvable.
    </p>
</section>

<?php include_once("footer.php"); ?>