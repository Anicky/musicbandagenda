<?php
$page_title = "Réinitialisation de votre mot de passe";
include_once("header.php");
?>
<section>
    <h1><?php echo $page_title; ?></h1>
    <?php
    if (isset($_GET['email'])) {
        $email = $_GET['email'];
        $password = $_GET['password'];
        if (($email != "") && ($password != "")) {
            $request_password = "SELECT id, password, firstname, lastname, email FROM members";
            $response_password = $db->query($request_password);
            $found = false;
            $id = "";
            while ((!$found) && ($data_password = $response_password->fetch())) {
                if ($email == (crypterSansSalt($data_password['email'])) && (testerMotDePasse($password, $data_password['password']))) {
                    $id = $data_password['id'];
                    $email = $data_password['email'];
                    $firstname = $data_password['firstname'];
                    $lastname = $data_password['lastname'];
                    $found = true;
                }
            }
            if ($found) {
                $password = chaineAleatoire(8);
                if (mail_password_reset($email, $firstname, $lastname, $password, get_parameter($db, "general_title"))) {
                    $request_reset = "UPDATE members SET password = ? WHERE id = ?";
                    $response_reset = $db->prepare($request_reset);
                    $response_reset->bindValue(1, crypter($password), PDO::PARAM_STR);
                    $response_reset->bindValue(2, $id, PDO::PARAM_INT);
                    $response_reset->execute();
                    $response_reset->closeCursor();
                    ?>
                    <p>
                        Votre mot de passe vient d'être réinitialisé !<br /><br />
                        Un mail vous a été envoyé pour que vous puissiez connaître votre nouveau de mot de passe.
                    </p>
                    <?php
                } else {
                    ?>
                    <p>Votre mot de passe n'a pas pu être réinitialisé.</p>
                    <?php
                }
            } else {
                ?>
                <p>Impossible de trouver votre compte : vérifiez que vous avez bien copié le lien de l'email que vous avez reçu.</p>
                <?php
            }
            $response_password->closeCursor();
        } else {
            header("Location: ./");
            exit;
        }
    } else {
        header("Location: ./");
        exit;
    }
    ?>

</section>
<?php
include_once("footer.php");
?>