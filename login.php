<?php
$page_title = "Connexion";
include_once("header.php");
?>

<section>
    <h1><?php echo $page_title; ?></h1>

    <form id="form_login">
        <table>
            <tr>
                <td class="label">
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" size="40" maxlength="100" class="validate[required]" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="password">Mot de passe</label>
                </td>
                <td>
                    <input type="password" name="password" id="password" size="40" maxlength="50" class="validate[required]" />
                </td>
            </tr>
        </table>
        <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>" />
        <input class="button" id="password" type="button" onclick="goTo('password_lost.html')" value="Mot de passe oublié" />
        <input class="button" type="submit" value="Valider" id="submit" />
    </form>
</section>

<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>

<script>
    $(function() {
        $("#form_login").validationEngine({
            scroll: false,
            onValidationComplete: function(form, status){
                if (status === true) {
                    $.ajax({
                        type : "post",
                        data : $("#form_login").serialize(),
                        url : "login_action.html",
                        success : function(data){
                            $("#dialogbox").html(data);
                            $("#dialogbox").dialog("open");
                        },
                        error : function(){
                            $("#dialogbox").html('Une erreur est survenue.');
                            $("#dialogbox").dialog("open");
                        }
                    });
                }
            } 
        });
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Connexion",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>

<?php include_once("footer.php"); ?>