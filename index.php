<?php
require_once("access.php");
$page_index = true;
include_once("header.php");
?>

<section>
    <?php
    $request_members = "SELECT id, firstname, lastname FROM members ORDER by firstname ASC, lastname ASC";
    $response_members = $db->prepare($request_members);
    $response_members->execute();
    while ($data_members = $response_members->fetch()) {
        ?>
        <span class="info user_<?php echo $data_members['id']; ?>"><?php echo $data_members['firstname'] . " " . strtoupper($data_members['lastname']); ?></span>
        <?php
    }
    $response_members->closeCursor();

    if ($session_role == "admin") {
        ?>
        <div class="right">
            <a href="javascript:void(0)" title="Rafraîchir le calendrier et supprimer les anciennes disponibilités" class="button" onclick="refresh()">
                <img src="img/icon_refresh.png" alt="" />
                Rafraîchir le calendrier
            </a>
        </div>
        <?php
    }
    ?>
</section>

<section>
    <div id="calendrier"></div>
</section>

<div id="dialogbox"></div>

<script>
    var token = '<?php echo $_SESSION['token_validation']; ?>';
    var calendar = null;
    $(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Créneaux horaires possibles",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
        calendar = $('#calendrier').fullCalendar({
            buttonText: {
                today: "Aujourd'hui",
                month: "Mois",
                week: "Semaine",
                day: "Jour"},
            defaultView: 'agendaWeek',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            selectable: true,
            theme : true,
            monthNames:
                ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
            monthNamesShort:
                ['janv.','févr.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'],
            dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            dayNamesShort: ['Dim','Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            titleFormat: {
                month: 'MMMM yyyy', // ex : Janvier 2010
                week: "d[ MMMM][ yyyy]{ - d MMMM yyyy}", // ex : 10 — 16 Janvier 2010, semaine à cheval : 28 Décembre 2009 - 3 Janvier 2010
                // todo : ajouter le numéro de la semaine
                day: 'dddd d MMMM yyyy' // ex : Jeudi 14 Janvier 2010
            },
            columnFormat: {
                month: 'ddd', // Ven.
                week: 'ddd d', // Ven. 15
                day: '' // affichage déja complet au niveau du 'titleFormat'
            },
            axisFormat: 'H:mm', // la demande de ferdinand.amoi : 15:00 (pour 15, simplement supprimer le ':mm'
            timeFormat: {
                '': 'H:mm', // événements vue mensuelle.
                agenda: 'H:mm{ - H:mm}' // événements vue agenda
            },
            firstDay:1, // Lundi premier jour de la semaine 
            selectHelper: true,
            select : function(start, end, allDay) {
                calendar.fullCalendar('unselect');
                $.ajax({
                    type: "post",
                    url: "agenda_setavailabilities.html",
                    data: 'start=' + $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss") +
                        '&end=' + $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss") +
                        '&token_validation=' + token,
                    success : function() {
                        calendar.fullCalendar('refetchEvents');
                    }
                });
            },
            eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
                $.ajax({
                    type: "post",
                    url: "agenda_setavailabilities.html",
                    data: 'id=' +event.id +
                        '&start=' + $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss") +
                        '&end=' + $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss") +
                        '&token_validation=' + token,
                    success : function() {
                        calendar.fullCalendar('refetchEvents');
                    }
                });
            },
            eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                $.ajax({
                    type: "post",
                    url: "agenda_setavailabilities.html",
                    data: 'id=' +event.id +
                        '&start=' + $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss") +
                        '&end=' + $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss") +
                        '&token_validation=' + token,
                    success : function() {
                        calendar.fullCalendar('refetchEvents');
                    }
                });
            },
            editable: true,
            allDayDefault : false,
            events: "agenda_getavailabilities.html",
            eventMouseover: function(event, domEvent) {
                if (event.member == '<?php echo $session_id; ?>') {
                    var layer = '<div id="events-layer_' + event.id + '" style="position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100;background:transparent;"><a><img src="img/icon_delete.png" alt="Supprimer" width="14" id="availability_delete_'+event.id+'" border="0" style="padding-right:5px; padding-top:2px;" /></a></div>';
                    $(this).append(layer);
                    $("#availability_delete_"+event.id).hide();
                    $("#availability_delete_"+event.id).fadeIn(300);
                    $("#availability_delete_"+event.id).click(function() {
                        $.ajax({
                            type: "post",
                            url: "agenda_delavailabilities.html",
                            data: 'id=' +event.id +
                                '&token_validation=' + token,
                            success : function() {
                                calendar.fullCalendar('refetchEvents');
                            }
                        });
                    });
                }
            },
            eventMouseout : function(event, domEvent) {
                if (event.member == '<?php echo $session_id; ?>') {
                    $("#events-layer_" + event.id).remove();
                }
            }
        });
    });
<?php
if ($session_role == "admin") {
    ?>
            function refresh() {
                $.ajax({
                    type: "post",
                    url: "agenda_refresh.html",
                    data: 'token_validation=' + token,
                    success : function() {
                        calendar.fullCalendar('refetchEvents');
                    }
                });
            }
    <?php
}
?>
</script>

<?php include_once("footer.php"); ?>