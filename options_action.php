<?php

require_once("access.php");
require_once("config.php");

if (isset($_POST['firstname'])) {
    // Variables
    $firstname = securite_bdd($_POST['firstname']);
    $lastname = securite_bdd($_POST['lastname']);
    $email = securite_bdd($_POST['email']);
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if (($firstname != "") && ($email != "")) {
            $request_options = "UPDATE members SET firstname = ?, lastname = ?, email = ? WHERE id = ?";
            $response_options = $db->prepare($request_options);
            $response_options->bindValue(1, $firstname, PDO::PARAM_STR);
            $response_options->bindValue(2, $lastname, PDO::PARAM_STR);
            $response_options->bindValue(3, $email, PDO::PARAM_STR);
            $response_options->bindValue(4, $session_id, PDO::PARAM_INT);
            $response_options->execute();
            $response_options->closeCursor();
            ?>
            <p>Les paramètres ont bien été sauvegardés.</p>
            <script>
                $("#dialogbox").bind('dialogclose', function() {
                    window.location.href = "options.html";
                });
            </script>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>