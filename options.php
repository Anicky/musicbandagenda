<?php
$page_title = "Paramètres";
require_once("access.php");
include_once("header.php");

$firstname = "";
$lastname = "";
$email = "";
$request_options = "SELECT firstname, lastname, email FROM members WHERE members.id = ?";
$response_options = $db->prepare($request_options);
$response_options->bindValue(1, $session_id, PDO::PARAM_INT);
$response_options->execute();
$data_options = $response_options->fetch();
if ($data_options != null) {
    $firstname = $data_options['firstname'];
    $lastname = $data_options['lastname'];
    $email = $data_options['email'];
}
$response_options->closeCursor();
?>

<section>
    <h1><?php echo $page_title; ?></h1>

    <form id="form_options">
        <table>
            <tr>
                <td class="label">
                    <label for="firstname">Prénom</label>
                </td>
                <td>
                    <input type="text" name="firstname" id="firstname" size="70" maxlength="100" class="validate[required]" value="<?php echo $firstname; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="firstname">Nom</label>
                </td>
                <td>
                    <input type="text" name="lastname" id="lastname" size="70" maxlength="100" value="<?php echo $lastname; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" size="70" maxlength="100" class="validate[required]" value="<?php echo $email; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label"></td>
                <td>
                    <input class="button" type="button" onclick="change_password()" value="Changer le mot de passe" id="password" />
                </td>
            </tr>
        </table>
        <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>"/>
        <input class="button" type="button" onclick="goTo('')" value="Annuler" id="back" />
        <input class="button" type="submit" value="Valider" id="submit" />
    </form>
</section>

<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>
<div id="dialogbox2"></div>

<script>
    function change_password() {
        $.post("password_change.html", {}, function(html) {
            $("#dialogbox2").dialog('option', 'buttons', {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            });
            $("#dialogbox2").html(html);
            $("#dialogbox2").dialog('open');
        });
    }
    $(function() {
        $("#form_options").validationEngine({
            scroll: false,
            onValidationComplete: function(form, status){
                if (status === true) {
                    $.ajax({
                        type : "post",
                        data : $("#form_options").serialize(),
                        url : "options_action.html",
                        success : function(data){
                            $("#dialogbox").html(data);
                            $("#dialogbox").dialog("open");
                        },
                        error : function(){
                            $("#dialogbox").html('Une erreur est survenue.');
                            $("#dialogbox").dialog("open");
                        }
                    });
                }
            } 
        });
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Paramètres",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialogbox2").dialog({
            autoOpen : false,
            modal : true,
            width : 700,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Mot de passe",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>

<?php include_once("footer.php"); ?>