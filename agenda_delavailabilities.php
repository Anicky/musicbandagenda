<?php

require_once("access.php");
require_once("config.php");

if (isset($_POST['id'])) {
    // Variables
    $id = $_POST['id'];
    // Traitement
    if ($_SESSION['token_validation'] == $_POST['token_validation']) {
        if ($id != "") {
            $ok = false;
            $request_availability = "SELECT member_fk FROM availabilities WHERE id = ?";
            $response_availability = $db->prepare($request_availability);
            $response_availability->bindValue(1, $id, PDO::PARAM_INT);
            $response_availability->execute();
            $data_availability = $response_availability->fetch();
            if ($data_availability != null) {
                echo $data_availability['member_fk'];
                if ($data_availability['member_fk'] == $session_id) {
                    $ok = true;
                }
            }
            $response_availability->closeCursor();
            if ($ok) {
                $request_availability = "DELETE FROM availabilities WHERE id = ?";
                $response_availability = $db->prepare($request_availability);
                $response_availability->bindValue(1, $id, PDO::PARAM_INT);
                $response_availability->execute();
                $response_availability->closeCursor();
            }
        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>