<?php
require_once("config.php");
if ((isset($_SESSION['user'])) && ($session_role == "admin") && (isset($_POST['id'])) && (isset($_POST['token_validation']))) {
    $id = $_POST['id'];
    if (($id != "") && ($_SESSION['token_validation'] == $_POST['token_validation'])) {

        $request_lives = "SELECT place, gain FROM lives WHERE id = ?";
        $response_lives = $db->prepare($request_lives);
        $response_lives->bindValue(1, $id, PDO::PARAM_INT);
        $response_lives->execute();
        $data_lives = $response_lives->fetch();
        if ($data_lives != null) {
            $place = securite_sortie($data_lives['place']);
            $gain = securite_sortie($data_lives['gain']);
            ?>
            <form id="form_live">
                <label for="place">Lieu</label><br />
                <input type="text" name="place" id="place" size="40" maxlength="100" value="<?php echo $place; ?>" /><br /><br />
                <label for="gain">Gains</label><br />
                <input type="number" name="gain" id="gain" size="10" maxlength="10" value="<?php echo $gain; ?>" /> €
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <input type="hidden" name="token_validation" value="<?php echo $_SESSION['token_validation']; ?>" />
            </form>
            <script>
                $("#dialogbox").dialog('option', 'buttons', { 
                    "Annuler" : function() {
                        $(this).dialog("close");
                    },
                    "Valider" : function() {
                        $("#form_live").submit();
                    }
                });
                $(function() {
                    $("#form_live").validationEngine({
                        scroll: false,
                        onValidationComplete: function(form, status){
                            if (status === true) {
                                $.ajax({
                                    type : "post",
                                    data : $("#form_live").serialize(),
                                    url : "lives_edit_action.html",
                                    success : function(data){
                                        $("#dialogbox").dialog('option', 'buttons', { 
                                            "Fermer" : function() {
                                                $(this).dialog("close");
                                            }
                                        });
                                        $("#dialogbox").html(data);
                                    },
                                    error : function(){
                                        $("#dialogbox").html('Une erreur est survenue.');
                                    }
                                });
                            }
                        } 
                    });
                });
            </script>
            <?php
        } else {
            ?>
            <p>Le concert recherché est introuvable.</p>
            <?php
        }
        $response_lives->closeCursor();
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php
    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
}
?>