<?php

session_start();
$session_started = true;
if (!isset($_SESSION['user'])) {
    header("Location: login.html");
    exit;
}
?>