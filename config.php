<?php

/* Parameters : DB, URL */
define("HOST", "localhost");
define("BASE", "musicbandagenda");
define("USER", "root");
define("PASS", "");
define("URL", "https://localhost/musicbandagenda/");
/* Parameters end */

if (!isset($session_started)) {
    session_start();
}
date_default_timezone_set('Europe/Paris');
$db = new PDO("mysql:host=" . HOST . ";dbname=" . BASE, USER, PASS);
$db->query("SET NAMES 'utf8'");
$session_id = "";
$session_role = "";
$session_firstname = "";
$session_lastname = "";
if (isset($_SESSION['user'])) {
    $request_user = "SELECT members.id AS user_id, roles.label AS user_role, firstname, lastname
        FROM members, roles
        WHERE members.role_fk = roles.id
        AND members.token = ?";
    $response_user = $db->prepare($request_user);
    $response_user->bindValue(1, $_SESSION['user'], PDO::PARAM_STR);
    $response_user->execute();
    $data_user = $response_user->fetch();
    if ($data_user != null) {
        $session_id = $data_user['user_id'];
        $session_role = $data_user['user_role'];
        $session_firstname = $data_user['firstname'];
        $session_lastname = $data_user['lastname'];
    }
    $response_user->closeCursor();
}
require_once("functions.php");
if (!isset($_SESSION['token_validation'])) {
    $_SESSION['token_validation'] = create_token();
}
?>