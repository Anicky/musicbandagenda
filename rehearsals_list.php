<?php
require_once("access.php");
if (!isset($db)) {
    require_once("config.php");
}

$members_count = 0;
$request_members = "SELECT COUNT(*) AS members_count FROM members";
$response_members = $db->query($request_members);
$data_members = $response_members->fetch();
if ($data_members != null) {
    $members_count = $data_members['members_count'];
}
$response_members->closeCursor();
?>

<h2>Répétitions prévues</h2>

<?php
$request_rehearsals = "SELECT id,
        DATE_FORMAT(date_start, '%d/%m/%Y') AS date_rehearsal,
        DATE_FORMAT(date_start, '%Hh%i') AS hour_start,
        DATE_FORMAT(date_end, '%Hh%i') AS hour_end,
        cost,
        place,
        TIME_FORMAT(TIMEDIFF(date_end, date_start), '%Hh%i') AS length
        FROM rehearsals WHERE date_start >= '" . date("Y-m-d H:i:s") . "' ORDER BY date_start ASC";
$response_rehearsals = $db->query($request_rehearsals);
$rehearsals_count = $response_rehearsals->rowCount();
if ($rehearsals_count > 0) {
    ?>
    <table class="cells">
        <tr>
            <th>Date</th>
            <th>Heure de début</th>
            <th>Heure de fin</th>
            <th id="place">Lieu</th>
            <th>Durée</th>
            <th>Coût total</th>
            <th>Coût par personne</th>
            <?php
            if ($session_role == "admin") {
                ?>
                <th></th>
                <th></th>
                <?php
            }
            ?>
        </tr>
        <?php
        while ($data_rehearsals = $response_rehearsals->fetch()) {
            ?>
            <tr>
                <td>
                    <?php echo $data_rehearsals['date_rehearsal']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['hour_start']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['hour_end']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['place']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['length']; ?>
                </td>
                <td>
                    <?php echo str_replace('.', ',', $data_rehearsals['cost']); ?> €
                </td>
                <td>
                    <?php echo str_replace('.', ',', ($data_rehearsals['cost'] / $members_count)); ?> €
                </td>
                <?php
                if ($session_role == "admin") {
                    ?>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Modifier la répétition" onclick="rehearsal_edit('<?php echo $data_rehearsals['id']; ?>')">
                            <img src="img/icon_edit.png" alt="" />
                        </a>
                    </td>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Supprimer la répétition" onclick="rehearsal_delete('<?php echo $data_rehearsals['id']; ?>')">
                            <img src="img/icon_delete.png" alt="" />
                        </a>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        $response_rehearsals->closeCursor();
        ?>
    </table>
    <?php
} else {
    ?>
    <p>Pas de répétitions prévues pour le moment.</p>
    <?php
}
?>

<h2>Répétitions passées</h2>

<?php
$request_rehearsals = "SELECT id,
        DATE_FORMAT(date_start, '%d/%m/%Y') AS date_rehearsal,
        DATE_FORMAT(date_start, '%Hh%i') AS hour_start,
        DATE_FORMAT(date_end, '%Hh%i') AS hour_end,
        cost,
        place,
        TIME_FORMAT(TIMEDIFF(date_end, date_start), '%Hh%i') AS length
        FROM rehearsals WHERE date_end < '" . date("Y-m-d H:i:s") . "' ORDER BY date_end DESC";
$response_rehearsals = $db->query($request_rehearsals);
$rehearsals_count = $response_rehearsals->rowCount();
if ($rehearsals_count > 0) {
    ?>
    <table class="cells">
        <tr>
            <th>Date</th>
            <th>Heure de début</th>
            <th>Heure de fin</th>
            <th id="place">Lieu</th>
            <th>Durée</th>
            <th>Coût total</th>
            <th>Coût par personne</th>
            <?php
            if ($session_role == "admin") {
                ?>
                <th></th>
                <th></th>
                <?php
            }
            ?>
        </tr>
        <?php
        while ($data_rehearsals = $response_rehearsals->fetch()) {
            ?>
            <tr>
                <td>
                    <?php echo $data_rehearsals['date_rehearsal']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['hour_start']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['hour_end']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['place']; ?>
                </td>
                <td>
                    <?php echo $data_rehearsals['length']; ?>
                </td>
                <td>
                    <?php echo str_replace('.', ',', $data_rehearsals['cost']); ?> €
                </td>
                <td>
                    <?php echo str_replace('.', ',', ($data_rehearsals['cost'] / $members_count)); ?> €
                </td>
                <?php
                if ($session_role == "admin") {
                    ?>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Modifier la répétition" onclick="rehearsal_edit('<?php echo $data_rehearsals['id']; ?>')">
                            <img src="img/icon_edit.png" alt="" />
                        </a>
                    </td>
                    <td class="icon">
                        <a href="javascript:void(0)" title="Supprimer la répétition" onclick="rehearsal_delete('<?php echo $data_rehearsals['id']; ?>')">
                            <img src="img/icon_delete.png" alt="" />
                        </a>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        $response_rehearsals->closeCursor();
        ?>
    </table>
    <?php
} else {
    ?>
    <p>Pas de répétitions passées.</p>
    <?php
}
?>
<h2>Coût des répétitions</h2>
<?php
$rehearsals_cost_total = 0;
$rehearsals_cost_unit = 0;
$request_rehearsals = "SELECT
        SUM(cost) AS cost_total
        FROM rehearsals
        WHERE date_end < '" . date("Y-m-d H:i:s") . "'";
$response_rehearsals = $db->query($request_rehearsals);
$data_rehearsals = $response_rehearsals->fetch();
if ($data_rehearsals != null) {
    $rehearsals_cost_total = $data_rehearsals['cost_total'];
    if ($rehearsals_cost_total == "") {
        $rehearsals_cost_total = 0;
    }
    $rehearsals_cost_unit = $rehearsals_cost_total / $members_count;
}
$response_rehearsals->closeCursor();
?>
<table>
    <tr>
        <td class="label">
            Coût total :
        </td>
        <td>
            <strong><?php echo str_replace('.', ',', $rehearsals_cost_total); ?> €</strong>
        </td>
    </tr>
    <tr>
        <td class="label">
            Coût par personne :
        </td>
        <td>
            <strong><?php echo str_replace('.', ',', $rehearsals_cost_unit); ?> €</strong>
        </td>
    </tr>
</table>