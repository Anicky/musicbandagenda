<?php

function securite_bdd($chaine) {
    return stripslashes($chaine);
}

/**
 * Sécurise une chaîne de caractères lorsqu'on souhaite l'afficher (évite les failles XSS)
 * @param $chaine La chaîne de caractères
 * @return La chaîne de caractères sécurisée
 */
function securite_sortie($chaine) {
    return htmlspecialchars($chaine);
}

/**
 * Créé une chaîne de caractères aléatoire (utile pour créer un mot de passe) en fonction d'un certain nombre de caractères.
 * @param Le nombre de caractères de la chaîne
 * @return La chaîne de caractères
 */
function chaineAleatoire($nombreCaracteres) {
    $password = "";
    $caracteres = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0, 1, 2, 3, 4, 5, 6,
        7, 8, 9, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    shuffle($caracteres);
    for ($i = 0; $i < $nombreCaracteres; $i++) {
        $password .= $caracteres[$i];
    }
    return $password;
}

function crypterSansSalt($chaine) {
    return sha1($chaine);
}

function crypterAvecSalt($chaine, $salt) {
    return sha1($chaine . $salt) . $salt;
}

function crypter($chaine) {
    return crypterAvecSalt($chaine, chaineAleatoire(8));
}

function testerMotDePasse($chaine, $motdepasse) {
    return crypterAvecSalt($chaine, substr($motdepasse, -8));
}

function create_token() {
    return crypterAvecSalt(time(), chaineAleatoire(8));
}

function get_parameter($db, $label) {
    $response_parameters = $db->prepare("SELECT value FROM parameters WHERE label = ?");
    $response_parameters->bindValue(1, $label, PDO::PARAM_STR);
    $response_parameters->execute();
    $data_parameters = $response_parameters->fetch();
    $value = $data_parameters['value'];
    $response_parameters->closeCursor();
    return $value;
}

function get_line_break($email) {
    $line_break = "\r\n";
    if (preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $email)) {
        $line_break = "\n";
    }
    return $line_break;
}

function get_mail_headers($email) {
    $line_break = get_line_break($email);
    $headers = "From: \"Jérémie Jalouzet\" <jeremie.jalouzet@gmail.com>" . $line_break;
    $headers .= "Reply-To: \"Jérémie Jalouzet\" <jeremie.jalouzet@gmail.com>" . $line_break;
    $headers .= "MIME-Version: 1.0" . $line_break;
    $headers .= "Content-Type: text/html; charset=utf-8" . $line_break;
    $headers .= "Content-Transfer-Encoding: 8bit" . $line_break;
    $headers .= "Date: " . date("D, j M Y H:i:s -0600") . $line_break;
    return $headers;
}

function mail_password_lost($email, $firstname, $lastname, $password, $general_title) {
    $firstname = securite_sortie($firstname);
    $lastname = securite_sortie($lastname);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $link = URL . "password_reset_" . crypterSansSalt($email) . "_" . crypter($password) . ".html";
    $message = "Bonjour " . $firstname . " " . strtoupper($lastname) . ",<br /><br />
        Ce mail vous est envoyé car vous avez oublié votre mot de passe.<br /><br />
        Si vous souhaitez réinitialiser votre mot de passe et en obtenir ainsi un nouveau,
        cliquez sur le lien suivant (ou faites un copier-coller du lien et inscrivez-le dans votre navigateur) :<br /><br />
        <a href=\"" . $link . "\" title=\"Réinitialisation de votre mot de passe\">" . $link . "</a><br /><br />
        Si ce mail vous a été envoyé par erreur, merci de ne pas en tenir compte et de le supprimer.<br /><br />
        L'administrateur de " . $general_title;
    $subject = "[" . $general_title . "] Mot de passe oublié";
    return mail($email, $subject, $message, get_mail_headers($email));
}

function mail_password_reset($email, $firstname, $lastname, $password, $general_title) {
    $firstname = securite_sortie($firstname);
    $lastname = securite_sortie($lastname);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $message = "Bonjour " . $firstname . " " . strtoupper($lastname) . ",<br /><br />
        Vous venez de demander à réinitialiser votre mot de passe. Voici vos identifiants : <br /><br />
        Votre email : " . $email . "<br />
        Votre mot de passe : " . $password . "<br /><br />
        Après vous être connecté, vous pourrez changer votre mot de passe dans les paramètres de votre compte.<br /><br />
        L'administrateur de " . $general_title;
    $subject = "[" . $general_title . "] Réinitialisation du mot de passe";
    return mail($email, $subject, $message, get_mail_headers($email));
}

function mail_password_change($email, $firstname, $lastname, $password, $general_title) {
    $firstname = securite_sortie($firstname);
    $lastname = securite_sortie($lastname);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $message = "Bonjour " . $firstname . " " . strtoupper($lastname) . ",<br /><br />
        Ce mail vous est envoyé car vous avez modifié votre mot de passe.<br /><br />
        Votre nouveau mot de passe est : " . $password . "<br /><br />
        Conservez-le précieusement, vous êtes le seul à connaître.<br /><br />
        Si vous l'oubliez, il vous sera toujours possible d'en récupérer un nouveau sur le site.<br /><br />
        L'administrateur de " . $general_title;
    $subject = "[" . $general_title . "] Mot de passe modifié";
    return mail($email, $subject, $message, get_mail_headers($email));
}

?>
